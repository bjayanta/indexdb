$(document).ready(function() {
    // console.log('ready');
    
    // open db
    var request = indexedDB.open('customers', 1);

    request.onupgradeneeded = function(e) {
        var db = e.target.result;

        if(!db.objectStoreNames.contains('customers')) {
            var os = db.createObjectStore('customers', {
                keypath: 'id',
                autoIncrement: true
            });
        }
    }

    // success 
    request.onsuccess = function(e) {
        console.log('Success: Opended DB.');
        db = e.target.result;

        // show customers
        // showCustomers();
    }

    // error 
    request.onerror = function(e) {
        console.log('Error: Could not open DB.');
    }

});

function addCustomer() {
    var transaction = db.transaction(['customers'], 'readwrite');

    // ask for objectstore
    var store = transaction.objectStore('customer');

    // define customer 
    var customer = {
        name: querySelector('#name').value,
        email: querySelector('#email').value
    }

    // perform the add
    var request = store.add(customer);

    // success 
    request.onsuccess = function(e) {
        window.location.href = 'index.html';
    }

    // error 
    request.onerror = function(e) {
        alert('Sorry, The customer was not added!');
        console.log('Error: ', e.target.error.name);
    }
}

